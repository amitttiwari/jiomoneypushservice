import sbt.Keys._

name := """jiomoney-pushservice"""

version := "1.0"

autoScalaLibrary := false

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "maven" at "https://repo1.maven.org/maven2/"
)

lazy val commonSettings = Seq(
  organization := "com.jio.money",
  version := "0.0.1",
  autoScalaLibrary := false,
  javaSource in Test := baseDirectory.value / "src" / "test" / "java",
  javaSource in Compile := baseDirectory.value / "src" / "main" / "java",
  ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) },
  scalaVersion := "2.11.7",
  libraryDependencies ++= Seq(
    // testing
    "junit"             % "junit"           % "4.12"  % Test,
    "com.novocode"      % "junit-interface" % "0.11"  % Test,
    "com.google.gcm" % "gcm-server" % "1.0.0",
    "com.notnoop.apns" % "apns" % "0.2.3",
    "com.google.code.gson" % "gson" % "2.7",
    javaWs
  )
)



lazy val pushService = (project in file("modules/services/pushservice")).
  settings(commonSettings: _*).
  enablePlugins(PlayJava)

lazy val pushApp = (project in file("modules/apps/pushnotification")).
  settings(commonSettings: _*).
  enablePlugins(PlayJava).
  dependsOn(pushService)


lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  enablePlugins(PlayJava).
  dependsOn(pushService,  pushApp)





