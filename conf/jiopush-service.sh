#!/bin/sh
SERVICE_NAME=wallet-push-service
JIOPUSH_SERVICE_PATH=/opt/wallet/jiomoney-pushservice-1.0
PID_PATH_NAME=$JIOPUSH_SERVICE_PATH//RUNNING_PID
WALLET_LOG_PATH=/opt/wallet/logs
WALLET_PUSH_PORT=3001
case $1 in
    start)
        echo "Starting $SERVICE_NAME ..."
        if [ ! -f $PID_PATH_NAME ]; then
            nohup ${JIOPUSH_SERVICE_PATH}/bin/jiomoney-pushservice -Dpidfile.path=${PID_PATH_NAME} -J-Dplay.crypto.secret="G48FR8Ky/GpF2Rj6uhCJ6bFU>bj^?gUdDBlkFXflAF" -J-Dplay.evolutions.enabled=false -Dhttp.port=${WALLET_PUSH_PORT} >${WALLET_LOG_PATH}/WALLET.log 2>${WALLET_LOG_PATH}/wallet.err &
            echo "$SERVICE_NAME started ..."
        else
            echo "$SERVICE_NAME is already running ..."
        fi
    ;;
    stop)
        if [ -f $PID_PATH_NAME ]; then
            PID=$(cat $PID_PATH_NAME);
            echo "$SERVICE_NAME stoping ..."
            kill $PID;
            echo "$SERVICE_NAME stopped ..."
            rm $PID_PATH_NAME
        else
            echo "$SERVICE_NAME is not running ..."
        fi
    ;;
    status)
        if [ -f $PID_PATH_NAME ]; then
            echo "$SERVICE_NAME running"
        else
            echo "$SERVICE_NAME is not running"
        fi
    ;;
    restart)
        if [ -f $PID_PATH_NAME ]; then
            PID=$(cat $PID_PATH_NAME);
            echo "$SERVICE_NAME stopping ...";
            kill $PID;
            echo "$SERVICE_NAME stopped ...";
            rm $PID_PATH_NAME
            echo "$SERVICE_NAME starting ..."
            nohup ${JIOPUSH_SERVICE_PATH}/bin/jiomoney-pushservice -Dpidfile.path=${PID_PATH_NAME} -J-Dplay.crypto.secret="G48FR8Ky/GpF2Rj6uhCJ6bFU>bj^?gUdDBlkFXflAF" -J-Dplay.evolutions.enabled=false -Dhttp.port=${WALLET_PUSH_PORT} >${WALLET_LOG_PATH}/WALLET.log 2>${WALLET_LOG_PATH}/wallet.err &
            echo "$SERVICE_NAME started ..."
else
echo "$SERVICE_NAME is not running ..."
fi
;;
esac
