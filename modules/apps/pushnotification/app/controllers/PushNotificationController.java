package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.jio.money.dto.PushNotificationDto;
import com.jio.money.service.PushNotificationService;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by amitmohantiwari on 06/09/16.
 */
@Singleton
public class PushNotificationController extends Controller {

    @Inject
    PushNotificationService pushSerivce;

    @Inject
    WSClient wsClient;

    Config config = ConfigFactory.load();
    private static Logger logger = LoggerFactory.getLogger(PushNotificationController.class);

    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> notifyUser() throws Exception {
        logger.info("inside notifyUser");
        PushNotificationDto request = Json.fromJson(request().body().asJson(), PushNotificationDto.class);
        logger.info("push notification request : {}", request.toString());
        String response = pushSerivce.notifyAppUser(request);

        return CompletableFuture.completedFuture(Results.ok(response));
    }

    public CompletionStage<Result> validateToken(String token) throws Exception {

        logger.info("inside validateToken: {}", token);
        String url = "https://" + config.getString("seco.host") + ":" + config.getString("seco.port") + config.getString("seco.context");
        logger.info(url);

        WSRequest request = wsClient.url(url).setHeader("Authorization", "Bearer " + token).setHeader("X-API-KEY", config.getString("api_key"));

        CompletionStage<WSResponse> response = request.get();
        return response.thenApply(resp -> {
            try {
                ObjectNode node = Json.newObject();
                if (resp.getStatus() == 200) {
                    JsonNode res = resp.asJson();
                    logger.info("got uuid {}", res.get("uuid"));
                    node.put("UUID", res.get("uuid"));
                    node.put("STATUS", "SUCCESS");
                } else {
                    logger.error("error case {} ",resp.asJson().toString());
                    node.put("STATUS", "ERROR");
                }
                return Results.ok(node);

            } catch (Exception ex) {
                logger.error("error {}", ex);
                ObjectNode node = Json.newObject();
                node.put("STATUS", "ERROR");

                return Results.ok(node);
            }
        }).exceptionally(ex -> {
            logger.error("error {}", ex);
            ObjectNode node = Json.newObject();
            node.put("STATUS", "ERROR");

            return Results.ok(node);
        });


    }

}
