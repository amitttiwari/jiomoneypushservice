package com.jio.money.dto;

import play.data.validation.Constraints;

import java.io.Serializable;

/**
 * Created by amitmohantiwari on 08/09/16.
 */
public class PushNotificationDto implements Serializable {

    @Constraints.Required
    private String pushId;

    @Constraints.Required
    private String type;

    @Constraints.Required
    private String data;

    @Constraints.Required
    private String titleMessage;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitleMessage() {
        return titleMessage;
    }

    public void setTitleMessage(String titleMessage) {
        this.titleMessage = titleMessage;
    }

    @Override
    public String toString() {
        return "PushNotificationDto{" +
                "data='" + data + '\'' +
                ", pushId='" + pushId + '\'' +
                ", type='" + type + '\'' +
                ", titleMessage='" + titleMessage + '\'' +
                '}';
    }
}
