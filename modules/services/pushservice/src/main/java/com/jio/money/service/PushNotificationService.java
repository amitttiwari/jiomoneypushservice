package com.jio.money.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Notification;
import com.google.android.gcm.server.Sender;
import com.jio.money.dto.PushNotificationDto;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;
import com.notnoop.apns.PayloadBuilder;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;

import play.libs.Json;
import play.libs.ws.WSClient;

import javax.inject.Inject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amitmohantiwari on 06/09/16.
 */
public class PushNotificationService {

    private static Logger logger = LoggerFactory.getLogger(PushNotificationService.class);

    Config config = ConfigFactory.load();

    public String notifyAppUser(PushNotificationDto requestDto) throws Exception {
        logger.info("notifyAppUser " + requestDto.getType());
        if (requestDto.getType().equalsIgnoreCase("GCM")) {

            String API_KEY = config.getString("android.api.key");
            Sender sender = new Sender(API_KEY);

            final List<String> regids = new ArrayList<String>();
            regids.add(requestDto.getPushId());
            //Notification notification = new Notification.Builder("").title(requestDto.getTitleMessage()).body(requestDto.getData()).build();

            Message pushMessage = new Message.Builder().timeToLive(30)
                    .delayWhileIdle(true).addData("data", requestDto.getData()).build();
                    //.notification(notification).build();

            logger.info("Push message " + pushMessage.toString());
            MulticastResult result = sender.send(pushMessage, regids, 0);
            logger.info("Android Message sent " + result);


        } else if (requestDto.getType().equalsIgnoreCase("APNS")) {
            ApnsServiceBuilder serviceBuilder = APNS.newService();
            InputStream certPath = Play.current().classloader().getResourceAsStream(config.getString("certificate.name"));
            serviceBuilder.withCert(certPath, config.getString("pushcert.password")).withProductionDestination();
            ApnsService service = serviceBuilder.build();
            String token = requestDto.getPushId();

            JsonNode dataJson = Json.parse(requestDto.getData());
            logger.info("dataJson {}", dataJson);
            PayloadBuilder payloadBuilder = APNS.newPayload().alertBody(requestDto.getTitleMessage()).badge(0).sound("noti.aiff");
            if(dataJson.hasNonNull("data")) {
                payloadBuilder.customField("data",dataJson.get("data"));

                if(dataJson.get("data").get("deeplink") != null) {
                    payloadBuilder.customField("deeplink", dataJson.get("data").get("deeplink"));
                }
            }
            if(dataJson.hasNonNull("event")) payloadBuilder.customField("event", dataJson.get("event"));
            if(dataJson.hasNonNull("type")) payloadBuilder.customField("type", dataJson.get("type"));
            if(dataJson.hasNonNull("tid")) payloadBuilder.customField("tid",dataJson.get("tid"));
            payloadBuilder.customField("mutable-content", 1);


            String payload = payloadBuilder.build();
            logger.info("payload {}", payload);

            logger.info("Pushing message to APN Server");
            service.push(token, payload);
            logger.info("APN Message Sent");

            //Sending notification with prod certification
            ApnsServiceBuilder prodServiceBuilder = APNS.newService();
            InputStream prodCertPath = Play.current().classloader().getResourceAsStream(config.getString("prod.certificate"));
            prodServiceBuilder.withCert(prodCertPath, config.getString("prod.pushcert.password")).withProductionDestination();
            ApnsService prodService = prodServiceBuilder.build();
            logger.info("Pushing message to APN Server with Prod certificate");
            prodService.push(token, payload);
            logger.info("APN Message Sent with Prod certificate");

        }
        return "sent";

    }
}
